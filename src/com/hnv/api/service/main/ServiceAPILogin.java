package com.hnv.api.service.main;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hnv.api.def.DefAPI;
import com.hnv.api.def.DefAPIExt;
import com.hnv.api.def.DefJS;
import com.hnv.api.main.API;
import com.hnv.api.main.SprB_Token;
import com.hnv.common.tool.ToolData;
import com.hnv.common.tool.ToolJSON;
import com.hnv.data.json.JSONObject;
import com.hnv.db.aut.TaAutUser;

/**
 * NVu.Hoang - Rin
 */

@RestController
@CrossOrigin
@RequestMapping(value = DefAPIExt.URL_API_LOGIN)
public class ServiceAPILogin {

	@Autowired
	private AuthenticationManager authenManager;

	@Autowired
	private SprB_Token token;

	@RequestMapping(method = RequestMethod.POST)
	protected void doGet(HttpServletRequest request, HttpServletResponse response)	throws Exception {
		JSONObject 		json 		= API.reqJson(request);
		String			uName		= ToolData.reqStr(json, DefJS.USER_NAME, "");
		String			uPass		= ToolData.reqStr(json, DefJS.USER_PASS	, "");
		
		doAuthenticate (uName, uPass);
		
		final String 	tokenStr 	= token.reqToken(uName);
		final TaAutUser uInf		= ServiceAPILoginCheck.reqAutUser(uName);
		API.doResponse(response, 
				ToolJSON.reqJSonString(
						DefJS.SESS_STAT	, 1, 
						DefJS.SV_CODE	, DefAPI.SV_CODE_API_YES,
						DefJS.USER_TOK	, tokenStr,
						DefJS.RES_DATA	, uInf
						));
	} 

	private void doAuthenticate(String username, String password) throws Exception {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try {
			authenManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
